# Отчёт по лабораторной работе №1
#### Нестеров Николай Константинович (P3230 | СИА3.2)

## Задание
1. Используйте метод K-средних и метод DBSCAN на самостоятельно сгенерированной выборке с количеством кластеров не менее 4. Для увеличения числа кластеров при генерации можно задать количество центров в функции make_blobs через параметр centers.
2. Используйте эти же два метода на датасете Mall_Customers.
3. Для каждого метода необходимо построить график.

## Выполнение
Всё начинается с импортов и настроек:
```python
from sklearn.cluster import KMeans, DBSCAN
from matplotlib.pyplot import style, rcParams, scatter, plot, show

style.use("ggplot")
rcParams["figure.figsize"] = (12, 8)
```

Затем, т.к. задания 1 и 2 выполняются одинаково, я вынес код, создающий все нужные графики, в функции

1. Для вывода датасета:
```python
def graph_for_dataset(X):
    return scatter(X[:, 0], X[:, 1])
```
2. Для создания модели метода KMeans:
```python
def create_and_fit_model(X, k: int) -> KMeans:
    kmeans_model = KMeans(n_clusters=k, random_state=13)
    kmeans_model.fit(X)
    return kmeans_model
```
3. Для создания графика метода локтя:
```python
def get_k_graph(X, max_k: int):
    criteries = [create_and_fit_model(X, k).inertia_ for k in range(2, max_k)]
    return plot(range(2, max_k), criteries)
```
4. Для создания графика с результатами кластеризации методом KMeans с найденным по методу локтя оптимальным k:
```python
def graph_for_kmeans(X, optimal_k: int):
    scatter(X[:, 0], X[:, 1], c=create_and_fit_model(X, optimal_k).labels_)
```
5. Для создания графика с результатами кластеризации методом DBSCAN:
```python
def graph_dbscan_results(eps: float, min_samples: int):
    return scatter(X[:, 0], X[:, 1], 
                   c=DBSCAN(eps=eps, min_samples=min_samples).fit_predict(X))
```
6. Для отображения графика и сообщения-заголовка:
```python
def display_graph(graph, name: str):
    print(name)
    show()
```

А также создал функцию, что вызывает все остальные в нужном порядке:
```python
def display_graphs(X, max_k: int, optimal_k: int, eps: float, min_samples: int):
    display_graph(graph_for_dataset(X), "Dataset: ")
    display_graph(get_k_graph(X, max_k), "График из метода локтя: ")
    display_graph(graph_for_kmeans(X, optimal_k), f"Кластеризация через kmeans (k={optimal_k}): ")
    display_graph(graph_dbscan_results(eps, min_samples), f"Кластеризация через DBSCAN (eps={eps}): ")
```

Теперь можно выполнить задания:
1. Инициализируем датасет:
Задание 1:
```python
from sklearn.datasets import make_blobs
X = make_blobs(n_samples=400, centers=6, random_state=13)[0]
```
Задание 2:
```python
from pandas import read_csv
from sklearn.preprocessing import StandardScaler

dataframe = read_csv("/content/drive/MyDrive/Colab Notebooks/Mall_Customers.csv", sep=",", header = 0)
dataframe = dataframe._get_numeric_data()
scaler = StandardScaler()
X = scaler.fit_transform(dataframe[["Annual Income (k$)", "Spending Score (1-100)"]]) 
```
2. Находим нужное значение k методом локтя (для обоих заданий это 5):
```python
display_graph(get_k_graph(X, 20), "График из метода локтя: ")
```
| Задание 1 | Задание 2 |
| --- | --- |
| ![img_2.png](img_2.png) |![img.png](img.png) |
3. Подбираем значения eps и min_samples вызывая функцию ниже с проверяемыми значениями eps и min_samples:
```python 
display_graph(graph_dbscan_results(eps, min_samples), f"Кластеризация через DBSCAN (eps={eps}): ")
```
4. На данном масштабе их просто подобрать вручную, для задания 1: `eps=1.5 min_samples=5`, для задания 2: `eps=0.35 min_samples=5`
5. Остаётся, зная оптимальные параметры, вызвать функцию, выводящую все графики и получить результаты:

| Метод | Задание 1 | Задание 2 |
| --- | --- | --- |
| KMeans | ![img_1.png](img_1.png) | ![img_4.png](img_4.png) |
| DBSCAN | ![img_3.png](img_3.png) | ![img_5.png](img_5.png) |

## Анализ результатов
### Задание 1
Как и ожидалось, на тестовых данных кластеризация обоими методами дала одинаковые результаты, хотя и поменяла цвета кластеров.

### Задание 2
На реальных же данных разные методы кластеризации скорее всего будут выдавать разные картинки. Так и случилось здесь:

KMeans сумел найти 5 чётких кластеров, в которых элементы находятся близко к центрам. 

DBSCAN же нашёл гораздо больше кластеров, а также оставил в стороне выбросы. Везде, где плотности точек не хватало для продолжения кластера, кластер прерывался. 
